package server

import (
	"errors"
	"flag"
	"fmt"
	"net"
	"net/http"
	"os"
	"strconv"

	"gitlab.com/S410/qrcv/logger"
)

var errorFailedToBind = errors.New("Failed to bind")

// Server is a wrapper around http.Server
type Server struct {
	http.Server

	// certificates
	useTLS  bool
	cert    string // Path to a TLS certificate file
	key     string // Path to a TLS key file
	certkey string // Path to both .crt and .key files, without extension

	// listening preferences
	v6             bool     // Use IPv6 too
	listenOn       string   // Address to listen on
	preferredPorts []string // Ports in order of preference

	// net stuff
	listener    net.Listener
	port        string   // the port TryListen settles on
	availableOn []string // list of URLs we're available on
}

func (s *Server) ConfigureFromArgs(argList []string) {
	parser := flag.NewFlagSet(os.Args[0], flag.ExitOnError)

	// Split into "addr" and "port" later
	var address string

	parser.StringVar(&address, "a", "", "Address")
	parser.StringVar(&s.cert, "c", "", "Certificate file")
	parser.StringVar(&s.key, "k", "", "Key file")
	parser.StringVar(
		&s.certkey, "ck", "",
		"Common certificate and key file prefix\n"+
			"-c and -k values are derived by appending .crt and .key to this value",
	)
	parser.BoolVar(&s.v6, "6", false, "Use IPv6")

	parser.Parse(argList)

	if s.certkey != "" {
		s.cert = s.certkey + ".crt"
		s.key = s.certkey + ".key"
	}

	if s.cert != "" && s.key != "" {
		s.useTLS = true
	}

	// Figure out address
	s.listenOn = address
	addr, port, err := net.SplitHostPort(address)
	if err == nil {
		s.listenOn = addr
	}

	// Figure out port(s) we want use, preferred first
	if port != "" {
		s.preferredPorts = []string{port}
	} else {
		s.preferredPorts = make([]string, 0, 13)

		if s.useTLS {
			s.preferredPorts = append(s.preferredPorts, "443", "8443")
		} else {
			s.preferredPorts = append(s.preferredPorts, "80")
		}

		for port := 8080; port <= 8090; port++ {
			s.preferredPorts = append(s.preferredPorts, strconv.Itoa(port))
		}
	}
}

// TryListen can be used to open a net.Listener before starting to serve clients
func (s *Server) TryListen() error {
	n := "tcp4"
	if s.v6 {
		n = "tcp"
	}

	for _, port := range s.preferredPorts {
		l, err := net.Listen(n, s.listenOn+":"+port)

		switch err := err.(type) {
		case *net.OpError:
			errorText := err.Err.Error()

			// Eat up non-fatal errors
			switch {
			case errorText == "bind: permission denied" && err.Op == "listen":
				logger.Printf("%s: permission denied", port)
				continue

			case errorText == "bind: address already in use":
				logger.Printf("%s: already in use", port)
				continue
			default:
				return err
			}
		}

		s.port = port
		s.listener = l

		return nil
	}

	return errorFailedToBind
}

func (s *Server) updateAvailableURLs() {
	if s.listener == nil {
		panic("updateAvailableURLs called before net.Listener is created!")
	}

	var addrs []string

	proto := "http"
	if s.useTLS {
		proto = "https"
	}

	if s.listenOn == "" {
		ips, _ := getOwnIps()

		for _, ip := range ips {
			if !s.v6 && ip.To4() == nil {
				continue
			}

			addrs = append(
				addrs,
				fmt.Sprintf("%s://%s:%s", proto, ip, s.port),
			)
		}
	} else {
		host := fmt.Sprintf("%s://%s:%s", proto, s.listenOn, s.port)
		addr := fmt.Sprintf("%s://%s", proto, s.listener.Addr().String())

		addrs = append(addrs, host)
		if host != addr {
			addrs = append(addrs, addr)
		}
	}

	s.availableOn = addrs
}

func (s *Server) GetURLs() []string {
	if s.availableOn == nil {
		s.updateAvailableURLs()
	}

	return s.availableOn
}

func (s *Server) Serve() error {
	if s.listener == nil {
		err := s.TryListen()
		if err != nil {
			return err
		}
	}

	if s.useTLS {
		return s.Server.ServeTLS(s.listener, s.cert, s.key)
	} else {
		return s.Server.Serve(s.listener)
	}
}
