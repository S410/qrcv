package main

import (
	"bufio"
	"embed"
	"errors"
	"fmt"
	"html/template"
	"io"
	"log"
	"mime/multipart"
	"net/http"
	"os"
	"path/filepath"
	"strconv"
	"strings"
	"time"

	"github.com/skip2/go-qrcode"
	"gitlab.com/S410/qrcv/logger"
	"gitlab.com/S410/qrcv/server"
)

var (
	//go:embed html/*
	htmlFs      embed.FS
	htmlMessage = template.Must(
		template.ParseFS(htmlFs, "html/page.html"),
	)
)

func handleHttp(w http.ResponseWriter, r *http.Request) {
	if r.URL.Path != "/" {
		http.Redirect(w, r, "/", http.StatusSeeOther)

		return
	}

	switch r.Method {
	case "GET":
		serveMessage(w, r, "center", "Select a file")
	case "POST":
		receiveFiles(w, r)
	}
}

func serveMessage(w http.ResponseWriter, _ *http.Request, class string, msg string) {
	w.WriteHeader(200)
	w.Header().Add("Content-Type", "text/html")

	err := htmlMessage.Execute(w, struct {
		Message      string
		MessageClass string
	}{msg, class})

	if err != nil {
		panic(err)
	}
}

func receiveFiles(w http.ResponseWriter, r *http.Request) {
	r.ParseMultipartForm(64 << 20)

	saved := make([]string, 0, len(r.MultipartForm.File))

	for _, file := range r.MultipartForm.File["file"] {
		filename, err := receiveFile(file)
		if err != nil {
			serveMessage(w, r, "err", err.Error())
		}

		saved = append(saved, filename)
	}

	serveMessage(w, r, "ok", strings.Join(saved, "\n"))
}

func receiveFile(f *multipart.FileHeader) (filename string, err error) {
	filename = strings.ReplaceAll(f.Filename, "/", "_")
	filename = findVacantFilepath(filename)

	incomingFile, err := f.Open()
	if err != nil {
		return "", err
	}

	localFile, err := os.Create(filename)
	if err != nil {
		return "", err
	}

	logger.Println("Receiving", filename)
	_, err = io.Copy(localFile, incomingFile)
	if err != nil {
		return "", err
	}

	logger.Println("Saved", filename)

	return filename, nil
}

var errorFileExits = errors.New("File exists")

func findVacantFilepath(ideal string) string {
	if fileExists(ideal) == nil {
		return ideal
	}

	basename := filepath.Base(ideal)
	ext := filepath.Ext(ideal)

	for i := 1; ; i++ {
		new := fmt.Sprintf("%s(%d)%s", basename, i, ext)

		if fileExists(new) == nil {
			return new
		}
	}
}

func fileExists(path string) error {
	_, err := os.Stat(path)
	if err == nil {
		return errorFileExits
	}

	if errors.Is(err, os.ErrNotExist) {
		return nil
	}

	return err
}

func printAsQr(s string) {
	qr, err := qrcode.New(s, qrcode.Low)
	if err != nil {
		panic(err)
	}

	qrs := qr.ToSmallString(false)

	// Force black & white colors
	fg := "\033[38;2;255;255;255m"
	bg := "\033[48;2;0;0;0m"
	f := fg + bg
	r := "\033[0m"

	//          Set color at the beginning of each line,
	//                                reset before each newline
	fmt.Println(f + strings.ReplaceAll(qrs, "\n", r+"\n"+f) + r)
}

func main() {
	server := &server.Server{}
	server.Server = http.Server{
		IdleTimeout: 15 * time.Second,
		Handler:     http.HandlerFunc(handleHttp),
	}

	server.ConfigureFromArgs(os.Args[1:])

	err := server.TryListen()
	if err != nil {
		log.Fatalln(err)
	}

	urls := server.GetURLs()

	fmt.Println("Listening on:")
	for i, url := range urls {
		fmt.Printf("  %d: %s\n", i+1, url)
	}

	go func() {
		logger.Fatalln(server.Serve())
	}()

	fmt.Printf("Pick address to print as QR code (1..%d): ", len(urls))
	lineScanner := bufio.NewScanner(os.Stdin)
	for lineScanner.Scan() {
		line := lineScanner.Text()
		n, err := strconv.Atoi(line)
		if err != nil || n < 1 || n > len(urls) {
			continue
		}

		printAsQr(urls[n-1])
	}
}
