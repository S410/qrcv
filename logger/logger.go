package logger

import (
	"log"
	"os"
)

var (
	logger = log.New(os.Stderr, "", 0)
)

func Println(v ...any) {
	logger.Println(v...)
}

func Printf(format string, v ...any) {
	logger.Printf(format, v...)
}

func Fatalln(v ...any) {
	logger.Fatalln(v...)
}
